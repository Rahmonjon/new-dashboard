import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';
import { Complaint } from 'src/app/models/complains';
import { BaseDataService } from 'src/app/services/base-data.service';
import * as moment from 'moment';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  complaints: Complaint[] = [];
  filtered_complaints: Complaint[] = [];
  filter_complaints: Complaint[] = [];
  totalcomplaints: number = 0;
  weeklycomplaints: number = 0;
  dailycomplaints: number = 0;

  constructor(private baseDataService: BaseDataService, private utilService: UtilService) {
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    let  start_date = moment().subtract(7, "days");
    let end_date = moment();
    this.complaints = await this.baseDataService.getComplaints();
    //below line for count the Total Complaints
    this.totalcomplaints = this.complaints.length;
    //below line for count the Total Daily
    this.dailycomplaints = this.complaints.filter(x =>moment(x.claim_date * 1000).format("YYYY-MM-DD") == end_date.format("YYYY-MM-DD")).length;
     //below line for count the Total Weekly
    this.filter_complaints = this.complaints.filter(x => moment(x.claim_date * 1000).format("YYYY-MM-DD") >= 
    start_date.format("YYYY-MM-DD") && moment(x.claim_date * 1000).format("YYYY-MM-DD") <= end_date.format("YYYY-MM-DD"))
    this.weeklycomplaints=this.filter_complaints.length;    
  }

}

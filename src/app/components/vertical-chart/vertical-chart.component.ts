import { Component, OnInit } from '@angular/core';
// for chart bellow https://stackblitz.com/edit/swimlane-grouped-vertical-bar-chart?embed=1&file=app/app.component.ts
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';



@Component({
  selector: 'app-vertical-chart',
  templateUrl: './vertical-chart.component.html',
  styleUrls: ['./vertical-chart.component.scss']
})
export class VerticalChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

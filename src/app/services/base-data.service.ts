import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Complaint, IComplaint } from '../models/complains';
@Injectable({
  providedIn: 'root'
})
export class BaseDataService {

  constructor(private http: HttpClient) {
  }

  async getComplaints(): Promise<any> {
    const result = await this.http.get<IComplaint[]>(`${environment.baseUrl}complaints`).toPromise();
    const res = result?.map((e: any) => e,new Complaint);
    return res;
  }

}

import * as moment from 'moment';

export interface IComplaint {
    id: number;
    product: string;
    reason: string;
    claim_date: number;
    product_name: string;
    full_date?: any;
}

export class Complaint implements IComplaint {
    id: number = 0;
    product: string = "";
    reason: string = "";
    claim_date: number = 0;
    get product_name(): string {
        if (this.product === "1") {
            return 'Minden';
        } else if (this.product === "2") {
            return 'Lübbecke';
        } else {
            return "Bad Oeynhausen";
        }
    }


    get full_date(): any {
        return moment(this.claim_date * 1000).format('YYYY-MM-DD');
    }
}

